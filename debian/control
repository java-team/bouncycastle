Source: bouncycastle
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends: ant,
               ant-optional,
               debhelper-compat (= 13),
               default-jdk,
               javahelper,
               junit4,
               libjakarta-activation-java,
               libjakarta-mail-java,
               libmail-java,
               maven-repo-helper
Standards-Version: 4.7.2
Vcs-Git: https://salsa.debian.org/java-team/bouncycastle.git
Vcs-Browser: https://salsa.debian.org/java-team/bouncycastle
Homepage: https://www.bouncycastle.org
Rules-Requires-Root: no

Package: libbcprov-java
Architecture: all
Depends: ${misc:Depends}
Breaks: jakarta-jmeter (<< 2.8-1~),
        jenkins-instance-identity (<< 1.3-1~),
        jglobus (<< 2.0.6-1~),
        voms-api-java (<< 2.0.9-1.1~)
Description: Bouncy Castle Java Cryptographic Service Provider
 The Bouncy Castle Crypto package is a Java implementation of
 cryptographic algorithms.
 .
 This package contains a JCE provider and a lightweight crypto API.

Package: libbcmail-java
Architecture: all
Depends: libbcpkix-java (>= ${source:Version}),
         libmail-java,
         ${misc:Depends}
Description: Bouncy Castle generators/processors for S/MIME and CMS
 The Bouncy Castle Crypto package is a Java implementation of
 cryptographic algorithms.
 .
 This package contains generators and processors for S/MIME and CMS
 (PKCS7/RFC 3852).

Package: libbcpkix-java
Architecture: all
Depends: libbcutil-java (>= ${source:Version}),
         ${misc:Depends}
Description: Bouncy Castle Java API for PKIX, CMS, EAC, TSP, PKCS, OCSP, CMP, and CRMF
 The Bouncy Castle Crypto package is a Java implementation of
 cryptographic algorithms.
 .
 This package contains generators and processors for PKIX, CMS,
 EAC, TSP, PKCS, OCSP, CMP, and CRMF.

Package: libbcpg-java
Architecture: all
Depends: libbcprov-java (>= ${source:Version}), ${misc:Depends}
Description: Bouncy Castle generators/processors for OpenPGP
 The Bouncy Castle Crypto package is a Java implementation of
 cryptographic algorithms.
 .
 This package contains generators and processors for OpenPGP (RFC 2440).

Package: libbctls-java
Architecture: all
Depends: libbcutil-java (>= ${source:Version}),
         ${misc:Depends}
Description: Bouncy Castle JSSE provider and TLS/DTLS API
 The Bouncy Castle Java APIs for TLS and DTLS, including a
 provider for the JSSE.

Package: libbcutil-java
Architecture: all
Depends: libbcprov-java (>= ${source:Version}), ${misc:Depends}
Multi-Arch: foreign
Description: Bouncy Castle ASN.1 Extension and Utility APIs
 The Bouncy Castle Java APIs for ASN.1 extension and utility APIs used
 to support bcpkix and bctls.

Package: libbcjmail-java
Architecture: all
Depends: libbcpkix-java (>= ${source:Version}),
         libjakarta-activation-java,
         libjakarta-mail-java,
         ${misc:Depends}
Description: Bouncy Castle Jakarta S/MIME API
 The Bouncy Castle Java S/MIME APIs for handling S/MIME protocols. This jar
 contains S/MIME APIs for JDK 1.5 and up. The APIs can be used in conjunction
 with a JCE/JCA provider such as the one provided with the Bouncy Castle
 Cryptography APIs. The Jakarta Mail API and the Jakarta activation framework
 will also be needed.
